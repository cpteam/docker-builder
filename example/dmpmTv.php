<?php

use CPTeam\Docker\Builder\Builder;

require __DIR__ . "/../vendor/autoload.php";

$builder = new Builder();

// Basic
$builder->setName("dmpm-tv")
	->setVsc("git", "git@gitlab.com:webgarden/dmpm-tv.git")
	->setComposeFile("./dockerCompose.yml");

// Container's
$builder->addContainer("dmpm-tv-app")
	->setDesc("PHP Container")
	->addEntry("php", "php console.php");

$builder->addContainer("dmpm-tv-nginx")
	->setDesc("Nginx Container");

$builder->addContainer("dmpm-tv-mysql")
	->setDesc("Mysql Container");

$builder->addContainer("dmpm-tv-node")
	->setDesc("Node Container")
	->addEntry("node", "npm")
	->addEntry("npm", "npm");

$builder->addContainer("dmpm-tv-redis")
	->setDesc("Redis Container")
	->addEntry("redis", "redis-cli");

// Options
$builder->addOption("build")
	->setAlias("b")
	->addHelper("build");

// Build
$builder->build();
