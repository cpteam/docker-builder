<?php

namespace CPTeam\Docker\Builder\Entity;

use CPTeam\Docker\Builder\Builder;

/**
 * @package Docker\Builder\Entity
 */
class Helper
{
	/** @var Builder */
	private $parent;
	
	/** @var string */
	private $name;
	
	/**
	 * @param Builder $parent
	 * @param string $name
	 */
	public function __construct(Builder $parent, $name)
	{
		$this->parent = $parent;
		$this->name = $name;
	}
	
	/**
	 * @return Builder
	 */
	public function getParent(): Builder
	{
		return $this->parent;
	}
	
	/**
	 * @param Builder $parent
	 *
	 * @return InputOption
	 */
	public function setParent(Builder $parent): InputOption
	{
		$this->parent = $parent;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}
	
	/**
	 * @param bool $break
	 *
	 * @return string
	 */
	public function getMethodName($break = true): string
	{
		return $this->getParent()->getId() . $this->getName() . ($break ? PHP_EOL : "");
	}
	
	/**
	 * @return string
	 */
	public function getContent(): string
	{
		return $this->getParent()->render("helper/{$this->getName()}");
	}
	
	/**
	 * @return string
	 */
	public function getMethod(): string
	{
		return $this->getMethodName(false) . "() {" . PHP_EOL . $this->getContent() . PHP_EOL . "}" . PHP_EOL;
	}
	
	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return $this->getContent() . PHP_EOL;
	}
}
