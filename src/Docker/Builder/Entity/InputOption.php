<?php

namespace CPTeam\Docker\Builder\Entity;

use CPTeam\Docker\Builder\Builder;

/**
 * @package Docker\Builder\Entity
 */
class InputOption
{
	/** @var Builder */
	private $parent;
	
	/** @var string */
	private $name;
	
	/** @var string */
	private $alias;
	
	/** @var array */
	private $helpers = [];
	
	/**
	 * @param Builder $parent
	 * @param string $name
	 */
	public function __construct(Builder $parent, $name)
	{
		$this->parent = $parent;
		$this->name = $name;
	}
	
	/**
	 * @return Builder
	 */
	public function getParent(): Builder
	{
		return $this->parent;
	}
	
	/**
	 * @param Builder $parent
	 *
	 * @return InputOption
	 */
	public function setParent(Builder $parent): InputOption
	{
		$this->parent = $parent;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}
	
	/**
	 * @param string $name
	 *
	 * @return InputOption
	 */
	public function setName(string $name): InputOption
	{
		$this->name = $name;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getAlias(): string
	{
		return $this->alias;
	}
	
	/**
	 * @param string $alias
	 *
	 * @return InputOption
	 */
	public function setAlias(string $alias): InputOption
	{
		$this->alias = $alias;
		
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getHelpers(): array
	{
		return $this->helpers;
	}
	
	/**
	 * @param array $helpers
	 *
	 * @return InputOption
	 */
	public function setHelpers(array $helpers): InputOption
	{
		$this->helpers = $helpers;
		
		return $this;
	}
	
	/**
	 * @param $helper
	 */
	public function addHelper($helper)
	{
		$this->helpers[] = $helper;
	}
	
	/**
	 * @param bool $skip
	 *
	 * @return string
	 */
	public function getVariable(bool $skip = false): string
	{
		if ($skip) {
			return "OPTION_" . strtoupper($this->getName());
		}
		
		return "\$OPTION_" . strtoupper($this->getName());
	}
	
	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return $this->getVariable();
	}
	
}
