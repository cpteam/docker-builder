<?php

namespace CPTeam\Docker\Builder\Entity;

use CPTeam\Docker\Builder\Builder;

/**
 * @package Docker\Builder\Entity
 */
class Action
{
	/** @var Builder */
	private $parent;
	
	/** @var string */
	private $name;
	
	/** @var bool */
	private $hidden = false;
	
	private $desc;
	
	/** @var Helper */
	private $helpers = [];
	
	/**
	 * @param Builder $parent
	 * @param string $name
	 */
	public function __construct(Builder $parent, $name)
	{
		$this->parent = $parent;
		$this->name = $name;
	}
	
	/**
	 * @return Builder
	 */
	public function getParent(): Builder
	{
		return $this->parent;
	}
	
	/**
	 * @param Builder $parent
	 *
	 * @return InputOption
	 */
	public function setParent(Builder $parent): InputOption
	{
		$this->parent = $parent;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}
	
	/**
	 * @param string $name
	 *
	 * @return InputOption
	 */
	public function setName(string $name): InputOption
	{
		$this->name = $name;
		
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getHelpers(): array
	{
		return $this->helpers;
	}
	
	/**
	 * @param $name
	 *
	 * @return Action
	 */
	public function addHelper($name): Action
	{
		$this->helpers[] = $this->getParent()->getHelper($name);
		
		return $this;
	}
	
	/**
	 * @return mixed
	 */
	public function getDesc()
	{
		return $this->desc;
	}
	
	/**
	 * @param $desc
	 *
	 * @return Action
	 */
	public function setDesc($desc): self
	{
		$this->desc = $desc;
		
		return $this;
	}
	
	public function setHidden()
	{
		$this->hidden = true;
	}
	
	/**
	 * @return bool
	 */
	public function isHidden(): bool
	{
		return $this->hidden;
	}
	
}
