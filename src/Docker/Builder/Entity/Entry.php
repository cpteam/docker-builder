<?php

namespace Docker\Builder\Entity;

/**
 * @package Docker\Builder\Entity
 */
class Entry
{
	private $name;
	private $command;
	
	/**
	 * @param $name
	 * @param $command
	 */
	public function __construct($name, $command)
	{
		$this->name = $name;
		$this->command = $command;
	}
	
	/**
	 * @return mixed
	 */
	public function getName(): string
	{
		return $this->name;
	}
	
	/**
	 * @return mixed
	 */
	public function getCommand(): string
	{
		return $this->command;
	}
	
	/**
	 * @param mixed $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}
	
	/**
	 * @param mixed $command
	 */
	public function setCommand(string $command)
	{
		$this->command = $command;
	}
}
