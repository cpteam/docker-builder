<?php

namespace CPTeam\Docker\Builder\Entity;

use CPTeam\Docker\Builder\Builder;
use Docker\Builder\Entity\Entry;

/**
 * @package Docker\Builder\Entity
 */
class Container
{
	/** @var Builder */
	private $parent;
	
	/** @var string */
	private $name;
	
	/** @var string */
	private $desc;
	
	/** @var array */
	private $entry = [];
	
	/**
	 * @param Builder $parent
	 * @param string $name
	 */
	public function __construct(Builder $parent, $name)
	{
		$this->parent = $parent;
		$this->name = $name;
	}
	
	/**
	 * @return string
	 */
	public function getDesc(): string
	{
		return $this->desc;
	}
	
	/**
	 * @param string $desc
	 *
	 * @return Container
	 */
	public function setDesc(string $desc): Container
	{
		$this->desc = $desc;
		
		return $this;
	}
	
	/**
	 * @return bool
	 */
	public function hasEntry(): bool
	{
		return $this->getEntry() != false;
	}
	
	/**
	 * @return Entry[]
	 */
	public function getEntry(): array
	{
		return $this->entry;
	}
	
	/**
	 * @param string $entry
	 * @param string $command
	 *
	 * @return Container
	 */
	public function addEntry(string $entry, string $command = null): Container
	{
		$this->entry[$entry] = new Entry($entry, $command);
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}
	
	public function __toString(): string
	{
		return $this->getName();
	}
	
}
