<?php

namespace CPTeam\Docker\Builder;

use Latte\Compiler;
use Latte\Macros\MacroSet;

/**
 * @package CPTeam\Docker\Builder
 */
class BashLatteMacros extends MacroSet
{
	/**
	 * @param Compiler $compiler
	 *
	 * @throws \InvalidArgumentException
	 */
	public static function install(Compiler $compiler)
	{
		$set = new static($compiler);
		$set->addMacro("red", "echo '\e[31m'");
		$set->addMacro("green", "echo '\e[32m'");
		$set->addMacro("yellow", "echo '\e[33m'");
		$set->addMacro("blue", "echo '\e[34m'");
		$set->addMacro("purple", "echo '\e[35m'");
		$set->addMacro("cyan", "echo '\e[36m'");
		$set->addMacro("grey", "echo '\e[37m'");
		$set->addMacro("darkGrey", "echo '\e[90m'");
		$set->addMacro("redBold", "echo '\e[1;31m'");
		$set->addMacro("greenBold", "echo '\e[1;32m'");
		$set->addMacro("yellowBold", "echo '\e[1;33m'");
		$set->addMacro("blueBold", "echo '\e[1;34m'");
		$set->addMacro("purpleBold", "echo '\e[1;35m'");
		$set->addMacro("cyanBold", "echo '\e[1;36m'");
		$set->addMacro("greyBold", "echo '\e[1;37m'");
		$set->addMacro("darkGreyBold", "echo '\e[1;90m'");
		$set->addMacro("end", "echo '\e[0m'");
		
	}
	
}
