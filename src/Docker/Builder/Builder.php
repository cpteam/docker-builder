<?php

namespace CPTeam\Docker\Builder;

use CPTeam\Docker\Builder\Entity\Action;
use CPTeam\Docker\Builder\Entity\Container;
use CPTeam\Docker\Builder\Entity\Helper;
use CPTeam\Docker\Builder\Entity\InputOption;
use Latte\Engine;

/**
 * @package CPTeam\Docker
 */
class Builder
{
	const VCS_GIT = "git";
	const VCS_SVN = "svn";
	
	const ACTION_INFO = "info";
	const ACTION_STATUS = "status";
	const ACTION_START = "start";
	const ACTION_STOP = "stop";
	const ACTION_RESTART = "restart";
	const ACTION_MAKE = "make";
	const ACTION_HELP = "help";
	
	const PATH_DEFAULT = "/build/";
	
	/** @var string */
	private $path = null;
	
	/** @var Engine */
	private $latte = null;
	
	private $uniq;
	
	/** @var array */
	private $config
		= [
			"name" => "",
			"composeFile" => "",
			"vsc" => [
				"type" => "",
				"url" => "",
			],
			"toolkitDir" => "/home/\$USER/dockerToolkit/",
			"projectDir" => null,
			"helpers" => [],
			"containers" => [],
			"actions" => [],
			"arguments" => [],
			"options" => [],
		];
	
	public function __construct()
	{
		$this->latte = new Engine();
		
		BashLatteMacros::install($this->latte->getCompiler());
		
		$this->uniq = uniqid();
		
		// Helpers
		$this->addHelper(self::ACTION_MAKE);
		$this->addHelper(self::ACTION_START);
		$this->addHelper(self::ACTION_STATUS);
		$this->addHelper(self::ACTION_STOP);
		$this->addHelper(self::ACTION_INFO);
		$this->addHelper(self::ACTION_HELP);
		
		// Base Action's
		$this->addAction(self::ACTION_HELP)
			->setDesc("Show help")
			->addHelper(self::ACTION_HELP);
		
		$this->addAction(self::ACTION_INFO)
			->setDesc("Show info about service")
			->addHelper(self::ACTION_INFO);
		
		$this->addAction(self::ACTION_STATUS)
			->setDesc("Show info about service")
			->addHelper(self::ACTION_STATUS);
		
		$this->addAction(self::ACTION_START)
			->setDesc("Start service")
			->addHelper(self::ACTION_START);
		
		$this->addAction(self::ACTION_STOP)
			->setDesc("Stop service")
			->addHelper(self::ACTION_STOP);
		
		$this->addAction(self::ACTION_RESTART)
			->setDesc("Restart service")
			->addHelper(self::ACTION_STOP)
			->addHelper(self::ACTION_START);
		
		$this->addAction(self::ACTION_MAKE)
			->setDesc("Make service")
			->addHelper(self::ACTION_MAKE);
		
	}
	
	/**
	 * @param string $type
	 * @param string $url
	 *
	 * @return Builder
	 */
	public function setVsc(string $type, string $url): Builder
	{
		$this->config['vsc']['type'] = $type;
		$this->config['vsc']['url'] = $url;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPath(): string
	{
		if ($this->path === null) {
			$this->path = $_SERVER["PWD"] . self::PATH_DEFAULT;
		}
		
		return $this->path;
	}
	
	/**
	 * @param string $path
	 *
	 * @return Builder
	 */
	public function setPath(string $path): Builder
	{
		$this->path = $path;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->config['name'];
	}
	
	/**
	 * @param string $name
	 *
	 * @return Builder
	 */
	public function setName(string $name): Builder
	{
		$this->config['name'] = $name;
		
		$this->config['projectDir'] = $this->config['toolkitDir'] . $name . DIRECTORY_SEPARATOR;
		
		return $this;
	}
	
	/**
	 * @param string $file
	 *
	 * @return Builder
	 */
	public function setComposeFile(string $file): Builder
	{
		$this->config['composeFile'] = $file;
		
		return $this;
	}
	
	// =================================================================================================
	// == [ Add Shortcut's ] ==
	// =================================================================================================
	
	/**
	 * @param string $name
	 * @param null $path
	 *
	 * @return Helper
	 */
	public function addHelper(string $name, $path = null): Helper
	{
		return $this->config['helpers'][$name] = new Helper($this, $name, $path);
	}
	
	/**
	 * @param string $name
	 *
	 * @return Container
	 */
	public function addContainer(string $name): Container
	{
		return $this->config['containers'][$name] = new Container($this, $name);
	}
	
	/**
	 * @param string $name
	 *
	 * @return Action
	 */
	public function addAction(string $name): Action
	{
		return $this->config['actions'][$name] = new Action($this, $name);
	}
	
	/**
	 * @param $name
	 *
	 * @return InputOption
	 */
	public function addOption($name): InputOption
	{
		return $this->config["options"][$name] = new InputOption($this, $name);
	}
	
	// =================================================================================================
	// == [ Render ] ==
	// =================================================================================================
	
	/**
	 * @param null $fileName
	 *
	 * @return int
	 */
	public function build(): int
	{
		$string = "";
		$string .= $this->render("header");
		$string .= $this->render("license");
		$string .= $this->render("config");
		
		/** @var Helper $helper */
		foreach ($this->config['helpers'] as $helper) {
			$string .= $helper->getMethod();
		}
		
		$string .= $this->render("init");
		
		$string .= $this->render("action");
		
		if (file_exists($this->getPath()) == false) {
			mkdir($this->getPath(), 0775);
		}
		
		file_put_contents($this->getPath() . "test.sh", $string);
		
		echo "\n\n====================================================\n";
		echo "$string";
		echo "\n\n====================================================\n";
		
		return 0;
	}
	
	public function render($name): string
	{
		$vars = [
			"config" => $this->config,
			"sys" => [
				"action" => "\$ACTION_" . $this->uniq,
				"helper" => $this->uniq . "_",
				"initFile" => ".initFileDockerBuilderToolkit",
			],
			
			"helpers" => $this->config['helpers'],
			"containers" => $this->config['containers'],
			"actions" => $this->config['actions'],
			"options" => $this->config['options'],
		];
		
		$str = __DIR__ . "/templates/$name.latte";
		
		$exists = file_exists($str) == false;
		
		if ($exists) {
			return "";
		}
		
		return $this->latte->renderToString($str, $vars);
		
	}
	
	/**
	 * @return string
	 */
	public function getId(): string
	{
		return $this->uniq;
	}
	
	/**
	 * @param $name
	 *
	 * @return Helper
	 */
	public function getHelper($name): Helper
	{
		return $this->config['helpers'][$name] ?? null;
	}
}
